package com.chuangs.custom.enclosurertype.statuscodes;

import com.chuangs.custom.utils.StringUtils;

import java.util.HashMap;
import java.util.Objects;

/**
 * Work happily and live happily ~~
 *
 * @Description:  消息提醒
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
public class CustomAjaxResultStatusCodes extends HashMap<String, Object>
{
    private static final long serialVersionUID = 1L;

    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MSG_TAG = "msg";

    /** 数据对象 */
    public static final String DATA_TAG = "data";

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public CustomAjaxResultStatusCodes()
    {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     */
    public CustomAjaxResultStatusCodes(int code, String msg)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    public CustomAjaxResultStatusCodes(int code, String msg, Object data)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (StringUtils.isNotNull(data))
        {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static CustomAjaxResultStatusCodes success()
    {
        return CustomAjaxResultStatusCodes.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static CustomAjaxResultStatusCodes success(Object data)
    {
        return CustomAjaxResultStatusCodes.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static CustomAjaxResultStatusCodes success(String msg)
    {
        return CustomAjaxResultStatusCodes.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static CustomAjaxResultStatusCodes success(String msg, Object data)
    {
        return new CustomAjaxResultStatusCodes(CustomHttpStatusStatusCodes.SUCCESS, msg, data);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static CustomAjaxResultStatusCodes warn(String msg)
    {
        return CustomAjaxResultStatusCodes.warn(msg, null);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static CustomAjaxResultStatusCodes warn(String msg, Object data)
    {
        return new CustomAjaxResultStatusCodes(CustomHttpStatusStatusCodes.WARN, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static CustomAjaxResultStatusCodes error()
    {
        return CustomAjaxResultStatusCodes.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 错误消息
     */
    public static CustomAjaxResultStatusCodes error(String msg)
    {
        return CustomAjaxResultStatusCodes.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 错误消息
     */
    public static CustomAjaxResultStatusCodes error(String msg, Object data)
    {
        return new CustomAjaxResultStatusCodes(CustomHttpStatusStatusCodes.ERROR, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg 返回内容
     * @return 错误消息
     */
    public static CustomAjaxResultStatusCodes error(int code, String msg)
    {
        return new CustomAjaxResultStatusCodes(code, msg, null);
    }

    /**
     * 是否为成功消息
     *
     * @return 结果
     */
    public boolean isSuccess()
    {
        return Objects.equals(CustomHttpStatusStatusCodes.SUCCESS, this.get(CODE_TAG));
    }

    /**
     * 是否为错误消息
     *
     * @return 结果
     */
    public boolean isError()
    {
        return !isSuccess();
    }

    /**
     * 方便链式调用
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public CustomAjaxResultStatusCodes put(String key, Object value)
    {
        super.put(key, value);
        return this;
    }
}