package com.chuangs.custom.test;

import com.chuangs.custom.entity.CustomTestPageSqlEntity;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 工具类
 * @Author: java开发-袁松
 * @Date: 2023/3/20
 **/
public class GetLists {


    public static Testpages  getInfo(List<?> lists) {
        Testpages testpages = new Testpages();
        testpages.setRows(lists);
        long total = new PageInfo<>(lists).getTotal();
        testpages.setTotal(total);
        return testpages;
    }
}
