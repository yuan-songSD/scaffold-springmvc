package com.chuangs.custom.enclosurertype;

import com.chuangs.custom.enclosurertype.page.CustomPageDomainPage;
import com.chuangs.custom.utils.ConvertUtils;
import com.chuangs.custom.utils.ServletUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Work happily and live happily ~~
 *
 * @Description:   表格处理 分页
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
@Slf4j
public class CustomTableSupportEnclosureType {
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 分页参数合理化
     */
    public static final String REASONABLE = "reasonable";

    /**
     * 封装分页对象
     */
    public static CustomPageDomainPage getPageDomain()
    {
        CustomPageDomainPage customPageDomainPage = new CustomPageDomainPage();

        customPageDomainPage.setPageNum(ConvertUtils.toInt(ServletUtils.getParameter(PAGE_NUM), 1));
        customPageDomainPage.setPageSize(ConvertUtils.toInt(ServletUtils.getParameter(PAGE_SIZE), 10));
        customPageDomainPage.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        customPageDomainPage.setIsAsc(ServletUtils.getParameter(IS_ASC));
        customPageDomainPage.setReasonable(ServletUtils.getParameterToBool(REASONABLE));
        return customPageDomainPage;
    }

    public static CustomPageDomainPage buildPageRequest()
    {
        return getPageDomain();
    }
}
