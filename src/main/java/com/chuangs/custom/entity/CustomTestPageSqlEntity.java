package com.chuangs.custom.entity;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.*;

/**
 * Work happily and live happily ~~
 *
 * @Description: 测试 分页是否需要链接数据 才能有效
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode
@ContentRowHeight(20)
@HeadRowHeight(20)
@ColumnWidth(25)
public class CustomTestPageSqlEntity {


    private int id;
    private String user_name;
    private String password;
    private String nick_name;
    private String phone;
    private Integer sex;
    private String address;
    private String avater;
    private Integer role_id;

}
