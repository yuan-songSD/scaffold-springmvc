package com.chuangs.custom.global;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import com.chuangs.custom.enclosurertype.statuscodes.CustomAjaxResultStatusCodes;
import com.chuangs.custom.enclosurertype.statuscodes.CustomHttpStatusStatusCodes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Work happily and live happily ~~
 *
 * @Description: 全局处理异常
 * @Author: java开发-袁松
 * @Date: 2023/3/11
 **/

@RestControllerAdvice
@Slf4j
public class CustomHandlerErrorGlobal extends RuntimeException {


    //捕获所有运行时异常
    @ExceptionHandler(RuntimeException.class)
    public CustomAjaxResultStatusCodes runtimeExceptionException(RuntimeException e) {
        log.error("RuntimeException", e);
        return CustomAjaxResultStatusCodes.error(CustomHttpStatusStatusCodes.RUN_ERROR, "运行时异常 详情请看当日日志");
    }

    /*数组越界异常*/
    @ExceptionHandler(ArrayIndexOutOfBoundsException.class)
    public CustomAjaxResultStatusCodes arrayIndexExceptionException(ArrayIndexOutOfBoundsException e) {
        log.error("ArrayIndexOutOfBoundsException", e);
        return CustomAjaxResultStatusCodes.error(CustomHttpStatusStatusCodes.ARRAY_TOP, "数组越界");
    }

    // 全局异常拦截OOOSS_569  未登录
    @ExceptionHandler(NotLoginException.class)
    public CustomAjaxResultStatusCodes NotLoginHandlerException(NotLoginException e) {

        log.error("当前会话未登录 请先登录", e);
        return CustomAjaxResultStatusCodes.error(CustomHttpStatusStatusCodes.NOT_LOGIN, "未登录");
    }

    // 没有权限  NotPermissionException
    @ExceptionHandler(NotPermissionException.class)
    public CustomAjaxResultStatusCodes NotPermissionExceptionHandlerException(NotPermissionException e) {

        log.error("没检测到当前用户有此权限 操作失败", e);
        return CustomAjaxResultStatusCodes.error(CustomHttpStatusStatusCodes.UNAUTHORIZED, "没有权限访问该接口");
    }
}
