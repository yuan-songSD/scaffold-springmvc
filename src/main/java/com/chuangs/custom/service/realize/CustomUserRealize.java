package com.chuangs.custom.service.realize;

import com.chuangs.custom.entity.CustomTestPageSqlEntity;
import com.chuangs.custom.hibernate.CustomUserHibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 用户实现层
 * @Author: java开发-袁松
 * @Date: 2023/3/14
 **/
@Service
public class CustomUserRealize {


    @Autowired
    CustomUserHibernate userHibernate;


    public List<CustomTestPageSqlEntity> lists() {
        return userHibernate.byIdQueryUser();
    }

}
