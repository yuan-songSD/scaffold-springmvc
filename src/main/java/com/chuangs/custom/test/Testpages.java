package com.chuangs.custom.test;

import com.chuangs.custom.enclosurertype.page.CustomTableDataInfoPage;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.Data;

import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description:
 * @Author: java开发-袁松
 * @Date: 2023/3/20
 **/
@Data
public class Testpages {

    private long total;
    private List<?> rows;


}
