package com.chuangs.custom.enclosurertype;

import com.chuangs.custom.enclosurertype.page.CustomTableDataInfoPage;
import com.chuangs.custom.enclosurertype.statuscodes.CustomAjaxResultStatusCodes;
import com.chuangs.custom.enclosurertype.statuscodes.CustomHttpStatusStatusCodes;
import com.chuangs.custom.utils.DateUtils;
import com.chuangs.custom.utils.PageUtils;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 接口继承该类 实现分页  web通用层
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
public class CustomBaseControllerEnclosureType {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        PageUtils.startPage();
    }

    /**
     * 清理分页的线程变量
     */
    protected void clearPage() {
        PageUtils.clearPage();
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected CustomTableDataInfoPage getDataTable(List<?> list) {
        CustomTableDataInfoPage rspData = new CustomTableDataInfoPage();
        rspData.setCode(CustomHttpStatusStatusCodes.SUCCESS);
        rspData.setdata(list);
        rspData.setMsg("查询成功");
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 返回成功
     */
    public CustomAjaxResultStatusCodes success() {
        return CustomAjaxResultStatusCodes.success();
    }

    /**
     * 返回成功消息
     */
    public CustomAjaxResultStatusCodes success(String message) {
        return CustomAjaxResultStatusCodes.success(message);
    }

    /**
     * 返回成功消息
     */
    public CustomAjaxResultStatusCodes success(Object data) {
        return CustomAjaxResultStatusCodes.success(data);
    }

    /**
     * 返回失败消息
     */
    public CustomAjaxResultStatusCodes error() {
        return CustomAjaxResultStatusCodes.error();
    }

    /**
     * 返回失败消息
     */
    public CustomAjaxResultStatusCodes error(String message) {
        return CustomAjaxResultStatusCodes.error(message);
    }

    /**
     * 返回警告消息
     */
    public CustomAjaxResultStatusCodes warn(String message) {
        return CustomAjaxResultStatusCodes.warn(message);
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected CustomAjaxResultStatusCodes toAjax(int rows) {
        return rows > 0 ? CustomAjaxResultStatusCodes.success() : CustomAjaxResultStatusCodes.error();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected CustomAjaxResultStatusCodes toAjax(boolean result) {
        return result ? success() : error();
    }
}
