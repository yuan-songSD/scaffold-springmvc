package com.chuangs.custom.hibernate;

import com.chuangs.custom.entity.CustomTestPageSqlEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 持久层 用户
 * @Author: java开发-袁松
 * @Date: 2023/3/14
 **/
@Repository
public interface CustomUserHibernate {


    List<CustomTestPageSqlEntity> byIdQueryUser();


}
