package com.chuangs.custom.utils;

import com.chuangs.custom.enclosurertype.CustomTableSupportEnclosureType;
import com.chuangs.custom.enclosurertype.page.CustomPageDomainPage;
import com.github.pagehelper.PageHelper;

/**
 * Work happily and live happily ~~
 *
 * @Description: 分页 工具类
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
public class PageUtils extends PageHelper {
    /**
     * 设置请求分页数据
     */
    public static void startPage() {
        CustomPageDomainPage customPageDomainPage = CustomTableSupportEnclosureType.buildPageRequest();
        Integer pageNum = customPageDomainPage.getPageNum();
        Integer pageSize = customPageDomainPage.getPageSize();
        String orderBy = SqlUtils.escapeOrderBySql(customPageDomainPage.getOrderBy());
        Boolean reasonable = customPageDomainPage.getReasonable();
        PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);
    }

    /**
     * 清理分页的线程变量
     */
    public static void clearPage() {
        PageHelper.clearPage();
    }
}