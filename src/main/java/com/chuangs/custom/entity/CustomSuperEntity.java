package com.chuangs.custom.entity;


import lombok.Data;

/*超类 所有实体都继承该类 里面有共同的字段 包含实现分页*/
@Data
public class CustomSuperEntity {

    public String createTime;
    public String modifyTime;
    public Integer pageNum;
    public Integer pageSize;

}



