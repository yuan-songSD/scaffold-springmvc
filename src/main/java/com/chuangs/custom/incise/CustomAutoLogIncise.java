package com.chuangs.custom.incise;

import java.lang.annotation.*;

/**
 * Work happily and live happily ~~
 *
 * @Description: 系统日志注解  实现日志管理功能
 * @Author: java开发-袁松
 * @Date: 2023/3/13
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomAutoLogIncise {

    /**
     * 日志内容
     *
     * @return
     */
    String value() default "";

    /**
     * 日志类型
     *
     * @return 1:登录日志;2:操作日志;3:访问日志;4:异常日志;5:定时任务;
     */
    int logType() default CustomCommonConstantIncise.LOG_TYPE_2;

    /**
     * 操作日志类型
     *
     * @return （1查询，2添加，3修改，4删除 5.导出 6.导入）
     */
    int operateType() default 0;
}

