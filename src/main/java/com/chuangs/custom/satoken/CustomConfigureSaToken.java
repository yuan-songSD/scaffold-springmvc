package com.chuangs.custom.satoken;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import com.chuangs.custom.config.CustomConfig;
import com.chuangs.custom.file.constant.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Work happily and live happily ~~
 *
 * @Description: 拦截器 实现 所以接口 需要携带token 才能访问{ 登录认证}
 * @Author: java开发-袁松
 * @Date: 2023/3/14
 **/
@Configuration
@Slf4j
public class CustomConfigureSaToken implements WebMvcConfigurer {
    // 注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，校验规则为 StpUtil.checkLogin() 登录校验。
        registry.addInterceptor(new SaInterceptor(handle -> StpUtil.checkLogin()))
                .addPathPatterns("/**")
                /*以下是不需要登录也能访问的接口*/
                .excludePathPatterns("/door/doLogin")
                .excludePathPatterns("/swagger-resources/**")
                .excludePathPatterns("/webjars/**")
                .excludePathPatterns("/v2/api-docs")
                .excludePathPatterns("/swagger-ui.html");

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        /** 本地文件上传路径
         *  profile 代替了  F:/customs/uploadPath
         * */
        registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**")
                .addResourceLocations("file:" + CustomConfig.getProfile() + "/");
    }
}
