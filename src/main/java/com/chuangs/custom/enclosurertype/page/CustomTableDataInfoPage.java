package com.chuangs.custom.enclosurertype.page;

import java.io.Serializable;
import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 分页的数据对象
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
public class CustomTableDataInfoPage implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 总记录数 */
    private long total;

    /** 列表数据 */
    private List<?> data;

    /** 消息状态码 */
    private int code;

    /** 消息内容 */
    private String msg;

    /**
     * 表格数据对象
     */
    public CustomTableDataInfoPage()
    {
    }

    /**
     * 分页
     *
     * @param list 列表数据
     * @param total 总记录数
     */
    public CustomTableDataInfoPage(List<?> list, int total)
    {
        this.data = list;
        this.total = total;
    }

    public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public List<?> getdata()
    {
        return data;
    }

    public void setdata(List<?> data)
    {
        this.data = data;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }
}