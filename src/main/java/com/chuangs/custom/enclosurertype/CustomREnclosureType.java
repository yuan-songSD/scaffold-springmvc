package com.chuangs.custom.enclosurertype;

import com.chuangs.custom.enclosurertype.statuscodes.CustomConstantsStatusCodes;

import java.io.Serializable;

/**
 * Work happily and live happily ~~
 *
 * @Description: 响应信息主体
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
public class CustomREnclosureType<T> implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 成功 */
    public static final int SUCCESS = CustomConstantsStatusCodes.SUCCESS;

    /** 失败 */
    public static final int FAIL = CustomConstantsStatusCodes.FAIL;

    private int code;

    private String msg;

    private T data;

    public static <T> CustomREnclosureType<T> ok()
    {
        return restResult(null, SUCCESS, null);
    }

    public static <T> CustomREnclosureType<T> ok(T data)
    {
        return restResult(data, SUCCESS, null);
    }

    public static <T> CustomREnclosureType<T> ok(T data, String msg)
    {
        return restResult(data, SUCCESS, msg);
    }

    public static <T> CustomREnclosureType<T> fail()
    {
        return restResult(null, FAIL, null);
    }

    public static <T> CustomREnclosureType<T> fail(String msg)
    {
        return restResult(null, FAIL, msg);
    }

    public static <T> CustomREnclosureType<T> fail(T data)
    {
        return restResult(data, FAIL, null);
    }

    public static <T> CustomREnclosureType<T> fail(T data, String msg)
    {
        return restResult(data, FAIL, msg);
    }

    public static <T> CustomREnclosureType<T> fail(int code, String msg)
    {
        return restResult(null, code, msg);
    }

    private static <T> CustomREnclosureType<T> restResult(T data, int code, String msg)
    {
        CustomREnclosureType<T> apiResult = new CustomREnclosureType<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }

    public static <T> Boolean isError(CustomREnclosureType<T> ret)
    {
        return !isSuccess(ret);
    }

    public static <T> Boolean isSuccess(CustomREnclosureType<T> ret)
    {
        return CustomREnclosureType.SUCCESS == ret.getCode();
    }
}