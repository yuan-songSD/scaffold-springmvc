package com.chuangs.custom.service;

import com.chuangs.custom.entity.CustomTestPageSqlEntity;

/**
 * Work happily and live happily ~~
 *
 * @Description: 业务层 。用户
 * @Author: java开发-袁松
 * @Date: 2023/3/14
 **/
public interface CustomUserService {


    CustomTestPageSqlEntity byIdQueryUser();
}
