package com.chuangs.custom.entity;

import lombok.Data;

/**
 * Work happily and live happily ~~
 *
 * @Description:
 * @Author: java开发-袁松
 * @Date: 2023/3/16
 **/
@Data
public class CustomTestPageEntity {

    public int id;
    public String name;
    public String address;
}
