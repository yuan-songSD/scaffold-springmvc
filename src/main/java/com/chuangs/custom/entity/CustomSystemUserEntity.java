package com.chuangs.custom.entity;

import lombok.Data;

/**
 * Work happily and live happily ~~
 *
 * @Description: 系统里的用户 实体类
 * @Author: java开发-袁松
 * @Date: 2023/3/13
 **/
@Data
public class CustomSystemUserEntity {


    private Long id;
    private String User_Name;
    private String Real_Name;
}
