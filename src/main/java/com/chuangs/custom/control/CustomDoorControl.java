package com.chuangs.custom.control;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.chuangs.custom.enclosurertype.CustomBaseControllerEnclosureType;
import com.chuangs.custom.enclosurertype.page.CustomTableDataInfoPage;
import com.chuangs.custom.enclosurertype.statuscodes.CustomAjaxResultStatusCodes;
import com.chuangs.custom.entity.CustomTestPageSqlEntity;
import com.chuangs.custom.excel.CustomToolsExcel;
import com.chuangs.custom.incise.CustomAutoLogIncise;
import com.chuangs.custom.incise.CustomCommonConstantIncise;
import com.chuangs.custom.service.realize.CustomUserRealize;
import com.chuangs.custom.test.GetLists;
import com.chuangs.custom.test.Testpages;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 提供 登录 和注册相关功能
 * @Author: java开发-袁松
 * @Date: 2023/3/13
 **/
@RequestMapping("/door")
@Api(tags = "登录/注册相关功能")
@RestController
@Slf4j
public class CustomDoorControl extends CustomBaseControllerEnclosureType {


    /*注入业务层对象*/
    @Autowired
    CustomUserRealize userRealize;


    @GetMapping("/doLogin")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "user_name", value = "账号由[由数字和密码组成]", required = true),
                    @ApiImplicitParam(name = "password", value = "密码由[由数字和密码组成]", required = true)
            }
    )
    @CustomAutoLogIncise(value = "用户登录", logType = CustomCommonConstantIncise.LOG_TYPE_1, operateType = 1)
    public CustomAjaxResultStatusCodes doLogin(String user_name, String password) {

        /*用户登录正确的是*/
        // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对
        // 第一步：比对前端提交的账号名称、密码
        if ("zhang".equals(user_name) && "123456".equals(password)) {
            // 第二步：根据账号id，进行登录
            StpUtil.login(10001);
            SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
            return CustomAjaxResultStatusCodes.success(tokenInfo);
        } else {
            return CustomAjaxResultStatusCodes.error("登录失败");

        }

    }


    @GetMapping("/userInfo")
    @CustomAutoLogIncise(value = "用户基本信息", logType = CustomCommonConstantIncise.LOG_TYPE_1, operateType = 1)
    @SaCheckPermission("user:delete")
    public CustomAjaxResultStatusCodes userInfo() {

        /*返回用户得所有基本信息*/

        return CustomAjaxResultStatusCodes.success("返回用户基本信息");
    }

    @GetMapping("/loginOut")
    @CustomAutoLogIncise()
    public CustomAjaxResultStatusCodes loginOut() {
        if (StpUtil.isLogin()) {
            StpUtil.logout();
            return CustomAjaxResultStatusCodes.success("操作成功");
        } else {
            return CustomAjaxResultStatusCodes.error("未登录");
        }
    }

    @GetMapping("/register")
    @CustomAutoLogIncise(value = "用户注册", logType = CustomCommonConstantIncise.LOG_TYPE_3, operateType = 2)
    public List<CustomTestPageSqlEntity> register(Integer pageNum, Integer pageSize) {

        PageHelper.startPage(pageNum, pageSize);
        List<CustomTestPageSqlEntity> lists = userRealize.lists();

        return lists;
    }

    @PostMapping("/export")
    @CustomAutoLogIncise(value = "导出", logType = CustomCommonConstantIncise.LOG_TYPE_2, operateType = 5)
    public void export(HttpServletResponse response) throws IOException {
        CustomToolsExcel.exports("测试表", userRealize.lists(), CustomTestPageSqlEntity.class, response);

    }

    @PostMapping("import")
    @CustomAutoLogIncise(value = "导入", logType = CustomCommonConstantIncise.LOG_TYPE_2, operateType = 6)
    public CustomAjaxResultStatusCodes imports(@RequestParam("file") File files) throws FileNotFoundException {
        List<CustomTestPageSqlEntity> imports = (List<CustomTestPageSqlEntity>) CustomToolsExcel.imports(files, CustomTestPageSqlEntity.class);
        return CustomAjaxResultStatusCodes.success(imports.size() + "解析出来的数据导入");

    }

}
