package com.chuangs.custom.control;

import com.chuangs.custom.config.CustomConfig;
import com.chuangs.custom.enclosurertype.statuscodes.CustomAjaxResultStatusCodes;
import com.chuangs.custom.file.FileUploadUtils;
import com.chuangs.custom.file.MimeTypeUtils;
import com.chuangs.custom.file.exception.InvalidExtensionException;
import com.chuangs.custom.incise.CustomAutoLogIncise;
import com.chuangs.custom.incise.CustomCommonConstantIncise;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Work happily and live happily ~~
 *
 * @Description:
 * @Author: java开发-袁松
 * @Date: 2023/3/31
 **/
@RestController
@RequestMapping("/file")
@Slf4j
@Api(tags = "文件管理/处理文件相关功能")
public class CustomFilesControl {


    @PostMapping("/upload")
    @CustomAutoLogIncise(value = "文件上传", logType = CustomCommonConstantIncise.LOG_TYPE_1, operateType = 1)
    public CustomAjaxResultStatusCodes upload(@RequestParam("avatarfile") MultipartFile file) throws IOException, InvalidExtensionException {
        //文件上传
        String avatar = FileUploadUtils.upload(CustomConfig.getUploadPath(), file, MimeTypeUtils.IMAGE_EXTENSION);

        return CustomAjaxResultStatusCodes.success(avatar);
    }
}
