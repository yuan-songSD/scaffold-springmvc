package com.chuangs.custom.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 监听 器 Excel 表格导入 工具类
 * @Author: java开发-袁松
 * @Date: 2023/3/24
 **/
public class CustomListenerExcel extends AnalysisEventListener<Object> {

    // 加入一个判断标签，判断数据是否已经读取完
    private volatile boolean retryLock = false;

    // 解析完成后的数据集合, 监听对象初始化之后，立即初始化集合对象
    private final List<Object> dataList = new ArrayList<>();

    // 每次最多导入条数
    private final int batchSize = 2000;


    @Override /*全部解析完成被调用*/
    public void doAfterAllAnalysed(AnalysisContext arg0) {
        retryLock = true;/*解锁*/
    }


    @Override /*解析 一行 调用一次  用于数据效验 后期对于数据要求高 可做扩展 每张表格不一样哎*/
    public void invoke(Object t, AnalysisContext analysisContext) {
        dataList.add(t);
    }

    /*获取数据解析好的数据*/
    public List<Object> getDataList() {
        while (true) {
            if (retryLock) {
                if (dataList.size() > batchSize) {
                    // 手动清空数据内存数据，减少内存消耗
                    dataList.clear();
                    throw new RuntimeException("一次最多导入" + batchSize + "条数据");
                } else {
                    return dataList;
                }
            }
        }
    }


}
