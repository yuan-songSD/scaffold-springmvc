package com.chuangs.custom.entity.aspect;

import lombok.Data;

import java.util.Date;

/**
 * Work happily and live happily ~~
 *
 * @Description: 日志实体类
 * @Author: java开发-袁松
 * @Date: 2023/3/13
 **/
@Data
public class CustomSystemLogAspect {


    private Long User_Id;
    private String User_Name;
    private String Real_Name;
    private String Ip;
    private String Request_Params; /*请求参数*/
    private Integer Log_Type; /*日志类型*/
    private String Log_Content;
    private String Method;
    private int Operate_Type;
    private long Cost_Time; /*耗时*/
    private Date Create_Time; /*创建时间*/
    private String Exception_Content;/*异常 信息内容 m描述*/
}
