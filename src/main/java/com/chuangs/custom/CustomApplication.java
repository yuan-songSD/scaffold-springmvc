package com.chuangs.custom;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@Slf4j
@SpringBootApplication
@MapperScan("com.chuangs.custom.hibernate")
public class CustomApplication {

    public static void main(String[] args) {

        SpringApplication.run(CustomApplication.class, args);

     log.info("===================启动=== 成功=============================");
    }

}
