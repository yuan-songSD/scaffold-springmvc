package com.chuangs.custom.satoken;

import cn.dev33.satoken.stp.StpInterface;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Work happily and live happily ~~
 *
 * @Description: 权限认证 ---菜单 精确到按钮级别
 * @Author: java开发-袁松
 * @Date: 2023/3/14
 **/

@Component    // 保证此类被SpringBoot扫描，完成Sa-Token的自定义权限验证扩展
public class CustomStpInterfaceImplSaToken implements StpInterface {

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 本list仅做模拟，实际项目中要根据具体业务逻辑来查询权限
        List<String> list = new ArrayList<String>();
       // list.add("user:delete");
        list.add("user.add");
        list.add("user.update");
        list.add("user.get");
        // list.add("user.delete");
        list.add("art.*");
        return list;
    }

    /**
     * @param o
     * @param s
     * @Description: 根据角色去做权限的判断
     * @return: java.util.List<java.lang.String>
     * @Author: ys
     * @Date: 2023/3/14
     */
    @Override
    public List<String> getRoleList(Object o, String s) {
        return null;
    }


}
